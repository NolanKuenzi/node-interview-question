const expect = require('expect');
const should = require('should');
const assert = require('assert');
const request = require('supertest');
const server = require("../bin/www");
const monk = require('monk');

const url = 'http://localhost:3000';

describe('Update User', function () {	
  it('updates the \'test user\' from the database', function (done) {
    const updatedUser = {
	  'username' : 'test user',
	  'email' : 'test3@test.com', //new email!
	  'fullname' : 'Bob Smith',
	  'age' : 27,
	  'location' : 'San Francisco',
	  'gender' : 'Male'
	};
	  const db = monk('localhost:27017/node_interview_question');
	  const collection = db.get('userlist');
	  collection.findOne({ 'username': 'test user' }, function (err, doc) {
	    const userId = doc._id;
      request(url)
	    .put('/users/updateuser/' + userId)
	    .send(updatedUser)
	    .expect(200)
	    .end(function (err, res) {
	      if (err) {
	        console.log(JSON.stringify(res));
	        throw err;
		    }
		    assert.equal(res.body.ok, 1)
	      db.close();
        done();
	    });
	  });
  });
  it('Character length exceeded', function(done) {
    const newUser = {
      'username' : 'Tell me and I’ll forget; show me and I may remember; involve me and I’ll understand.–Ancient Chinese proverb',
	  'email' : 'test1@test.com',
	  'fullname' : 'Bob Smith',
	  'age' : 27,
	  'location' : 'San Francisco',
	  'gender' : 'Male'
	};
	request(url)
	  .post('/users/adduser')
	  .send(newUser)
	  .expect(400)
	  .end(function (err, res) {
	    assert.equal(res.body, 'Username character limit of 100 has been exceeded')
		if (err) {
		  console.log(JSON.stringify(res));
		  throw err;
		}
		  done();
	  });
  });
});