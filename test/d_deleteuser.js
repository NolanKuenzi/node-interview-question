const expect = require('expect');
const should = require('should');
const assert = require('assert');
const request = require('supertest');
const server = require("../bin/www");
const monk = require('monk');

const url = 'http://localhost:3000';

describe('Delete User', function () {
  it('deletes the \'test user\' from the database', function (done) {
	  const db = monk('localhost:27017/node_interview_question');
	  const collection = db.get('userlist');
	  collection.findOne({ 'username': 'test user' }, function (err, doc) {
	    const userId = doc._id;
	    request(url)
	    .delete('/users/deleteuser/' + userId)
	    .expect(200)
	    .end(function (err, res) {
	      if (err) {
		      console.log(JSON.stringify(res));
		      throw err;
				}
				assert.equal(res.body.ok, 1)
        db.close();
	      done();
       });
     });
  });
});