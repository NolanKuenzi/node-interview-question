const expect = require('expect');
const should = require('should');
const assert = require('assert');
const request = require('supertest');
const server = require("../bin/www");
const monk = require('monk');

const url = 'http://localhost:3000';

describe('Gets all users', function() {
	  /* drop db after tests */
  it('Gets all users', function(done) {
	  request(url)
	  .get('/users/userlist')
		.expect(200)
		.end(function(err, res) {
		  if (err) {
		    console.log(JSON.stringify(res));
			  throw err;
			}
			assert.equal(res.body[res.body.length -1].username, 'test user');
			assert.equal(res.body[res.body.length -1].email, 'test1@test.com');
			assert.equal(res.body[res.body.length -1].fullname, 'Bob Smith');
			assert.equal(res.body[res.body.length -1].age, '27');
			assert.equal(res.body[res.body.length -1].location, 'San Francisco');
			assert.equal(res.body[res.body.length -1].gender, 'Male');
			done();
		});
	});
});