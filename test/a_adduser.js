const expect = require('expect');
const should = require('should');
const assert = require('assert');
const request = require('supertest');
const server = require("../bin/www");
const monk = require('monk');

const url = 'http://localhost:3000';

describe('Add User', function() {
  it('Adds a new user with user name \'test user\'', function(done) {
    const newUser = {
		'username' : 'test user',
		'email' : 'test1@test.com',
		'fullname' : 'Bob Smith',
		'age' : 27,
		'location' : 'San Francisco',
		'gender' : 'Male'
		};
	request(url)
	  .post('/users/adduser')
	  .send(newUser)
	  .expect(200)
	  .end(function (err, res) {
	    if (err) {
				console.log(JSON.stringify(res));
				throw err;
			}
			assert.equal(res.body.username, 'test user')
			assert.equal(res.body.email, 'test1@test.com')
			assert.equal(res.body.fullname, 'Bob Smith')
			assert.equal(res.body.age, '27')
			assert.equal(res.body.location, 'San Francisco')
			assert.equal(res.body.gender, 'Male')
		done();
	  });
	});
  it('Missing form field', function(done) {
    const newUser = {
	    'username' : 'test user',
	    'email' : 'test1@test.com',
	    'fullname' : '',
	    'age' : 27,
	    'location' : 'San Francisco',
	    'gender' : 'Male'
  	};
      request(url)
	    .post('/users/adduser')
	    .send(newUser)
	    .expect(400)
	    .end(function (err, res) {
		    if (err) {
				  console.log(JSON.stringify(res));
					throw err;
				}
	      assert.equal(res.body, 'Please fill out all required fields')
		    done();
	    });
	});
	it('Character length exceeded', function(done) {
	  const newUser = {
		  'username' : 'Tell me and I’ll forget; show me and I may remember; involve me and I’ll understand.–Ancient Chinese proverb',
		  'email' : 'test1@test.com',
		  'fullname' : 'Bob Smith',
		  'age' : 27,
		  'location' : 'San Francisco',
		  'gender' : 'Male'
	  };
	  request(url)
	  .post('/users/adduser')
	  .send(newUser)
	  .expect(400)
	  .end(function (err, res) {
		  if (err) {
		    console.log(JSON.stringify(res));
			 throw err;
		  }
	    assert.equal(res.body, 'Username character limit of 100 has been exceeded')
		  done();
		});
	});
});