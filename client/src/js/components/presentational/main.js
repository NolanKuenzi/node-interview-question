import React from 'react';
import Header from './header';
import UserData from '../container/userData';

const Main = () => (
  <div>
    <Header />
    <UserData />
  </div>
);

export default Main;
