import React, { useState, useEffect } from 'react';
import axios from 'axios';
import regeneratorRuntime, { async } from 'regenerator-runtime';
import trStyle from '../../componentStyles';

const UserData = () => {
  const [username, setUserName] = useState('');
  const [email, setEmail] = useState('');
  const [fullname, setFullName] = useState('');
  const [age, setAge] = useState('');
  const [location, setLocation] = useState('');
  const [gender, setGender] = useState('');
  const [getData, setGetData] = useState(true);
  const [userInfo, setUserInfo] = useState(null);
  const [userList, setUserList] = useState(null);
  const [arrow, setArrow] = useState('▼');
  const [userStatus, setUserStatus] = useState('add');

  const submitFunc = async event => {
    event.preventDefault();
    /* post request */
    if (userStatus === 'add') {
      /* prevent duplicate usernames */
      if (userList !== null) {
        const dupUsers = userList.filter(item => item.username === username);
        if (dupUsers.length !== 0) {
          alert('Username already taken');
          return;
        }
      }
      try {
        const request = await axios.post('http://localhost:3000/users/adduser', {
          username,
          email,
          fullname,
          age,
          location,
          gender,
        });
        if (request.status === 200) {
          setUserName('');
          setEmail('');
          setFullName('');
          setAge('');
          setLocation('');
          setGender('');
          setGetData(true);
        }
      } catch (error) {
        if (error.response !== undefined) {
          alert(error.response.data);
          return;
        }
        alert(error);
      }
    }
    /* put request */
    if (userStatus === 'update') {
      if (userInfo === null) {
        return;
      }
      try {
        const request = await axios.put(`http://localhost:3000/users/updateuser/${userInfo._id}`, {
          username: username === '' ? userInfo.username : username,
          email: email === '' ? userInfo.email : email,
          fullname: fullname === '' ? userInfo.fullname : fullname,
          age: age === '' ? userInfo.age : age,
          location: location === '' ? userInfo.location : location,
          gender: gender === '' ? userInfo.gender : gender,
        });
        if (request.status === 200) {
          setUserName('');
          setEmail('');
          setFullName('');
          setAge('');
          setLocation('');
          setGender('');
          setGetData(true);
        }
      } catch (error) {
        if (error.response !== undefined) {
          alert(error.response.data);
        }
        alert(error);
      }
    }
  };
  /* toggle between adding or updating a user */
  const toggleUsers = () => {
    if (userStatus === 'add') {
      setUserStatus('update');
    } else {
      setUserStatus('add');
    }
  };
  /* toggle user list */
  const toggleArrow = () => {
    if (arrow === '▼') {
      setArrow('▲');
    } else {
      setArrow('▼');
    }
  };
  const userInfoFunc = id => {
    const usrInfo = userList.filter(item => item._id === id);
    setUserInfo(usrInfo[0]);
  };
  const deleteFunc = async event => {
    try {
      const request = await axios.delete(
        `http://localhost:3000/users/deleteuser/${event.target.id}`
      );
      if (request.status === 200) {
        setGetData(true);
      }
    } catch (error) {
      if (error.response !== undefined) {
        alert(error.response.data);
        return;
      }
      alert(error);
    }
  };
  /* get request */
  useEffect(() => {
    if (getData === true) {
      try {
        const data = async () => {
          const request = await axios.get('http://localhost:3000/users/userlist');
          setUserInfo(request.data.length === 0 ? null : request.data[request.data.length - 1]);
          setUserList(request.data.length === 0 ? null : request.data.slice(0));
          setGetData(false);
        };
        data();
      } catch (error) {
        if (error.response !== undefined) {
          setGetData(false);
          return;
        }
        setGetData(false);
      }
    }
  }, [getData]);
  return (
    <div>
      <div id="topSection">
        <div id="userInfo">
          <h3>User Info</h3>
          {userInfo === null ? null : (
            <div>
              <span>Name: {userInfo === null ? null : userInfo.fullname}</span>
              <br />
              <span>Age: {userInfo === null ? null : userInfo.age}</span>
              <br />
              <span>Gender: {userInfo === null ? null : userInfo.gender}</span>
              <br />
              <span>Location: {userInfo === null ? null : userInfo.location}</span>
              <br />
              <div id="usrInfoBtnDiv">
                <button type="button" onClick={() => toggleUsers()}>
                  Update User
                </button>
              </div>
            </div>
          )}
        </div>
        <div id="addUserDiv">
          <h3>
            {userStatus === 'add'
              ? 'Add User:'
              : `Update User: ${userInfo === null ? '' : userInfo.username}`}
          </h3>
          {userStatus === 'add' ? null : (
            <button type="button" onClick={() => toggleUsers()}>
              Cancel
            </button>
          )}
          <form onSubmit={event => submitFunc(event)}>
            {userStatus === 'update' ? <input readOnly></input> : null}
            {userStatus === 'update' ? null : (
              <input
                type="text"
                placeholder="Username"
                value={username}
                onChange={event => setUserName(event.target.value)}
              ></input>
            )}
            <input
              type="text"
              placeholder="Email"
              value={email}
              onChange={event => setEmail(event.target.value)}
            ></input>
            <br />
            <input
              type="text"
              placeholder="Full Name"
              value={fullname}
              onChange={event => setFullName(event.target.value)}
            ></input>
            <input
              type="text"
              placeholder="Age"
              value={age}
              onChange={event => setAge(event.target.value)}
            ></input>
            <br />
            <input
              type="text"
              placeholder="Location"
              value={location}
              onChange={event => setLocation(event.target.value)}
            ></input>
            <input
              type="text"
              placeholder="Gender"
              value={gender}
              onChange={event => setGender(event.target.value)}
            ></input>
            <br />
            <button type="submit" id="submitBtn">
              Submit
            </button>
          </form>
        </div>
      </div>
      <div id="userList">
        <div id="toggle" onClick={() => toggleArrow()}>
          <h3>
            User List: <span id="arrow">{arrow === '▼' ? '▼' : '▲'}</span>
          </h3>
        </div>
        <table>
          <tbody>
            <tr>
              <th>UserName</th>
              <th>Email</th>
              <th>Delete</th>
            </tr>
            {userList === null || arrow === '▼'
              ? null
              : userList.map((item, index) => (
                  <tr
                    key={`tr-${index}`}
                    style={userInfo !== null && userInfo._id === item._id ? trStyle : null}
                  >
                    <td
                      key={`uName-${index}`}
                      id={item._id}
                      onClick={event => userInfoFunc(event.target.id)}
                    >
                      {item.username}
                    </td>
                    <td
                      key={`eMail-${index}`}
                      id={item._id}
                      onClick={event => userInfoFunc(event.target.id)}
                    >
                      {item.email}
                    </td>
                    <td id="delBtnTd" key={`del-${index}`}>
                      <button
                        type="button"
                        id={item._id}
                        className="delBtn"
                        key={`delBtn-${index}`}
                        onClick={event => deleteFunc(event)}
                      >
                        Delete
                      </button>
                    </td>
                  </tr>
                ))}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default UserData;
