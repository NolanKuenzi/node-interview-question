const express = require("express");
const path = require("path");
const favicon = require("serve-favicon");
const logger = require("morgan");
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const cors = require('cors');
// Database
const monk = require("monk");
const db = monk("localhost:27017/node_interview_question");

const routes = require("./routes/index");
const users = require("./routes/users");

const app = express();

app.use('/views', express.static(process.cwd() + '/views'));
app.use('/public', express.static(process.cwd() + '/public'));

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));

app.use(logger("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(cors());

// Make our db accessible to our router
app.use(function(req, res, next) {
  req.db = db;
  next();
});

app.use("/", routes);
app.use("/users", users);

/// catch 404 and forwarding to error handler
app.use(function(req, res, next) {
  const err = new Error("Not Found");
  err.status = 404;
  next(err);
});

/// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(500).json('Internal Server Error');
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(500).json('Internal Server Error');
});

module.exports = app;
