const express = require('express');
const router = express.Router();
const { body, sanitizeBody, validationResult } = require('express-validator');


/*
 * GET userlist.
 */
router.get('/userlist', function(req, res) {
    const db = req.db;
    const collection = db.get('userlist');
    collection.find({}, function(err, docs) {
      if (err) {
        res.status(500).json('Internal Server Error');
      }
      res.status(200).json(docs);
    });
});

/*
 * POST to adduser.
 */
router.post('/adduser', [
  body('username')
    .not().isEmpty().withMessage('Please fill out all required fields')
    .trim()
    .isLength({max: 100}).withMessage('Username character limit of 100 has been exceeded'),
  sanitizeBody('username')
    .escape(),
  body('email')
    .not().isEmpty().withMessage('Please fill out all required fields')
    .trim()
    .isLength({max: 100}).withMessage('Email character limit of 100 has been exceeded'),
  sanitizeBody('email')
    .escape(),
  body('fullname')
    .not().isEmpty().withMessage('Please fill out all required fields')
    .trim()
    .isLength({max: 100}).withMessage('Fullname character limit of 100 has been exceeded'),
  sanitizeBody('fullname')
    .escape(),
  body('age')
    .not().isEmpty().withMessage('Please fill out all required fields')
    .trim()
    .isLength({max: 100}).withMessage('Age character limit of 100 has been exceeded'),
  sanitizeBody('age')
    .escape(),
  body('location')
    .not().isEmpty().withMessage('Please fill out all required fields')
    .trim()
    .isLength({max: 100}).withMessage('Location character limit of 100 has been exceeded'),
  sanitizeBody('location')
    .escape(),
  body('gender')
    .not().isEmpty().withMessage('Please fill out all required fields')
    .trim()
    .isLength({max: 100}).withMessage('Gender character limit of 100 has been exceeded'),
  sanitizeBody('gender')
    .escape(),
], function(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        res.status(400).json(errors.array()[0].msg);
        return;
    }
    const db = req.db;
    const collection = db.get('userlist');
    const {username, email, fullname, age, location, gender} = req.body;
    const newUser = {username: username, email: email, fullname: fullname, age: age, location: location, gender: gender};
    collection.insert(newUser, function(err, result) {
        if (err) {
          res.status(500).json('Internal Server Error');
        }
        res.status(200).json(result);
    });
});

router.put('/updateuser/:id', [
  body('username')
    .trim()
    .isLength({max: 100}).withMessage('Username character limit of 100 has been exceeded'),
  sanitizeBody('username')
    .escape(),
  body('email')
    .trim()
    .isLength({max: 100}).withMessage('Email character limit of 100 has been exceeded'),
  sanitizeBody('email')
    .escape(),
  body('fullname')
    .trim()
    .isLength({max: 100}).withMessage('Fullname character limit of 100 has been exceeded'),
  sanitizeBody('fullname')
    .escape(),
  body('age')
    .trim()
    .isLength({max: 100}).withMessage('Age character limit of 100 has been exceeded'),
  sanitizeBody('age')
    .escape(),
  body('location')
    .trim()
    .isLength({max: 100}).withMessage('Location character limit of 100 has been exceeded'),
  sanitizeBody('location')
    .escape(),
  body('gender')
    .trim()
    .isLength({max: 100}).withMessage('Gender character limit of 100 has been exceeded'),
  sanitizeBody('gender')
    .escape(),
], function(req, res) {
   const errors = validationResult(req);
   if (!errors.isEmpty()) {
      res.status(400).json(errors.array()[0].msg);
      return;
  }
    const db = req.db;
    const collection = db.get('userlist');
    const {username, email, fullname, age, location, gender} = req.body;
    collection.update({_id: req.params.id}, {$set: {username: username, email: email, fullname: fullname, age: age, location: location, gender: gender}}, function(err, result) {
      if (err) {
        res.status(500).json('Internal Server Error');
      }
      res.status(200).json(result);
    })
})

/*
 * DELETE to deleteuser.
 */
router.delete('/deleteuser/:id', function(req, res) {
    const db = req.db;
    const collection = db.get('userlist');
    const userToDelete = req.params.id;
    collection.remove({ '_id' : userToDelete }, function(err, doc) {
      if (err) {
        res.status(500).json('Internal Server Error');
      }
      res.status(200).json(doc.result);
    });
});

module.exports = router;